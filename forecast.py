import sys
import datetime
import dateutil.parser
import json
import warnings
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

from fbprophet import Prophet
from statsmodels.tsa.arima_model import ARIMA

# json.loads => 문자열을 객체로
# obj = json.loads(sys.argv[1])

warnings.filterwarnings('ignore')

def ema(df, step):
    df['y'] = df['y'].ewm(step).mean()

def forecast(config, obj):
    timeList = obj["timeList"]
    # 시간을 datetime 형식으로 변경
    timeList = [datetime.datetime.strptime(time, "%Y%m%d%H%M%S") for time in timeList]
    dataList = obj["dataList"]
    forecastStep = int(1440 / config['aggregation_time'])

    if config['algorithm'] == "prophet":
        # 입력 데이터를 데이터프레임화
        df = pd.DataFrame(data={"ds": timeList, "y": dataList})
        df["ds"] = df["ds"].astype("datetime64[ns]")

        # 지수 이동 평균 5 적용
        ema(df, 5)

        if config['aggregation_function'] == "sum":
            df['cap'] = 100 * config['aggregation_time']
        elif config['aggregation_function'] == "count":
            df['cap'] = config['aggregation_time']
        else:
            df["cap"] = 100
        df["floor"] = 0

        m = Prophet(growth="logistic", yearly_seasonality=False,
                    daily_seasonality=config["daily_seasonality"],
                    weekly_seasonality=config["weekly_seasonality"],
                    changepoint_prior_scale=config["trend_sensitivity"])

        m.fit(df)
        future = m.make_future_dataframe(periods= forecastStep, freq=str(config['aggregation_time'])+ "min")

        future["cap"] = 100
        future["floor"] = 0

        forecast = m.predict(future)

        # forecast 데이터프레임의 컬럼들
        # ['ds', 'trend', 'cap', 'floor', 'yhat_lower', 'yhat_upper','trend_lower', 'trend_upper', 'additive_terms',
        # 'additive_terms_lower','additive_terms_upper', 'daily', 'daily_lower', 'daily_upper', 'weekly','weekly_lower', 'weekly_upper',
        # 'multiplicative_terms','multiplicative_terms_lower', 'multiplicative_terms_upper', 'yhat']

        returnTimeList = (forecast["ds"][-forecastStep:]).tolist()
        # 판다스의 Timestamp 타입은 곧바로 JSON으로 만들 수 없어서 문자열화
        returnTimeList = [time.strftime("%Y%m%d%H%M%S") for time in returnTimeList]

        returnDataList = (forecast["yhat"][-forecastStep:]).tolist()
        returnDataUpperList = (forecast["yhat_upper"][-forecastStep:]).tolist()
        returnDataLowerList = (forecast["yhat_lower"][-forecastStep:]).tolist()

        returnObj = {
            "returnTimeList": returnTimeList,
            "returnDataList": returnDataList,
            "returnDataUpperList": returnDataUpperList,
            "returnDataLowerList": returnDataLowerList
        }

        return returnObj

    elif config['algorithm'] == "ARIMA":
        # 입력 데이터를 데이터프레임화
        df = pd.DataFrame(data={"ds": timeList, "y": dataList})

        # 지수 이동 평균 5 적용
        ema(df, 5)

        # 출력용 시간을 미리 생성
        startTime = timeList[0] + datetime.timedelta(minutes=config['aggregation_time'] * forecastStep)
        ranges = pd.date_range(start=startTime, periods=forecastStep, freq=str(config['aggregation_time']) + "min")
        dtList = [dt.to_pydatetime() for dt in ranges]
        returnTimeList = [time.strftime("%Y%m%d%H%M%S") for time in dtList]

        model = ARIMA(df["y"], order=(config["p"], config["d"], config["q"]))
        model_fit = model.fit(trend='c', full_output=False, disp=1)

        fore = model_fit.forecast(steps=forecastStep)

        returnObj = {
            "returnTimeList": returnTimeList,
            "returnDataList": fore[0].tolist(),
            "returnDataUpperList": [data[0] for data in fore[2].tolist()],
            "returnDataLowerList": [data[1] for data in fore[2].tolist()]
        }
        return returnObj
