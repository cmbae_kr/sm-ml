import warnings
import numpy as np
from sklearn.cluster import DBSCAN
warnings.filterwarnings('ignore')

"""
넘어오는 json 형식
{
    "data": {
        "unit_id": RESOURCE(float),
        "unit_id": RESOURCE(float),
        ...
    },
    "m": int,
    "e": int
}
"""

def outlier(obj):
    #unit_id와 리소스 데이터의 순서성을 유지하면서 각 일차원 리스트로 변환
    unitIDList=[]
    resourceList=[]
    resultObj = {
        "isOutlier": False
    }
    for unit_id in obj["data"]:
        unitIDList.append(unit_id)
        resourceList.append(obj["data"][unit_id])
    # 리소스들을 대상으로 1차원 DBSCAN 적용
    dbscan = DBSCAN(min_samples=obj["m"], eps=obj["e"])
    cluster = dbscan.fit_predict(X=np.reshape(np.array(resourceList), newshape=(-1, 1)))
    cluster = cluster.tolist()

    for i in range(len(unitIDList)):
        tempObj = {
            "value":resourceList[i],
            "isOutlier": False
        }
        if cluster[i] == -1:
            tempObj["isOutlier"] = True
            resultObj["isOutlier"] = True
        else:
            tempObj['isOutlier'] = False

        resultObj[unitIDList[i]] = tempObj

    return resultObj
