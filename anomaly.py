import warnings
import numpy as np
from sklearn.cluster import DBSCAN
warnings.filterwarnings('ignore')

"""
넘어오는 json 형식
{
    "timeList": [...],
    "dataList": [...],
    "interval": int
}
"""
def ema(df, step):
    df['y'] = df['y'].ewm(step).mean()

def anomaly(config, todayObj, pastObjList):

    pastDataList = []

    returnObj = {
        "upper": 0,
        "lower": 0,
        "isAnomaly": False
    }

    for obj in pastObjList:
        pastDataList.append(obj['dataList'])

    dbscan = DBSCAN(min_samples=2, eps=config['tolerance'])
    cluster = dbscan.fit_predict(np.reshape(pastDataList,newshape=(-1, 1)))
    cluster = cluster.tolist()

    clusteredList = []

    for idx in range(len(cluster)):
        if cluster[idx] == 0:
            clusteredList.append(pastDataList[idx])
        else:
            continue

    #만약 군집이 만들어졌다면
    if len(clusteredList)>0:
        returnObj['upper'] = max(clusteredList) + config['tolerance']
        returnObj['lower'] = min(clusteredList) - config['tolerance']

    #만약 군집이 만들어지지 않았다면
    else:
        returnObj['upper'] = (sum(clusteredList) / len(clusteredList)) + config['tolerance']
        returnObj['lower'] = (sum(clusteredList) / len(clusteredList)) - config['tolerance']

    #오늘자 데이터가 이상치인지 판단
    if todayObj['dataList'] <= returnObj['upper'] and todayObj['dataList']>=returnObj['lower']:
        returnObj['isAnomaly'] = False
    else:
        returnObj['isAnomaly'] = True

    returnObj["real_value"] = todayObj['dataList']

    return returnObj
