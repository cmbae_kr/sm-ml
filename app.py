import requests
from apscheduler.schedulers.background import BackgroundScheduler
import datetime
from flask import Flask, request, jsonify
from elasticsearch import Elasticsearch, helpers
import elasticsearch
import json
import pandas as pd
import numpy as np
import forecast
import anomaly
import outlier
from pytz import timezone
from pytz import utc

app = Flask(__name__)
es = Elasticsearch("192.168.1.60", http_auth=("elastic", "!1q2w3e4r5t"))
configObj = {}


def stringToDate(str):
    return datetime.datetime.strptime(str, "%Y%m%d%H%M%S")


def dateToString(date):
    return date.strftime("%Y%m%d%H%M%S")


@app.route("/ml/save_score")
def saveScore():
    runningDate = (datetime.datetime.now(timezone("UTC")) - datetime.timedelta(days=1)).strftime("%Y%m%d")
    # 모든 ML Job 설정을 가져옴
    res = requests.get("http://192.168.1.60:9999/lcnet/ml/all_config")
    allConfig = res.json()

    try:
        for config in allConfig:
            job_type = config['job_type']
            if config['job_state'] == "off":
                continue
            else:
                if job_type == "forecast":
                    forecastScore(config, runningDate)
                elif job_type == "anomaly":
                    anomalyScore(config, runningDate)
                elif job_type == "outlier":
                    outlierScore(config, runningDate)
    except Exception as e:
        print(e)
        return jsonify({"result": False})

    return jsonify({"result": True})


# 예측 ML Job의 RMSE를 저장하는 메소드
def forecastScore(config, runningDate):
    yesterday = dateToString(stringToDate(runningDate) - datetime.timedelta(days=1))
    increaseRate = 0

    # runningDate 날짜의 rmse 가져오기
    # 반환 형식
    # {"rmse": {rmse}}
    res = requests.get(
        "http://192.168.1.60:9999/lcnet/ml/rmse?date=" + runningDate + "&job_id=" + config['job_id'] + "&unit_id=" +
        config['unit_id'] + "&field=" + config['field'] +
        "&aggregation_function=" + config['aggregation_function'] + "&aggregation_time=" + config['aggregation_time'])
    res = res.json()
    runningDateRmse = res["rmse"]

    # runningDate -1일 날짜의 rmse 가져오기
    res = requests.get(
        "http://192.168.1.60:9999/lcnet/ml/rmse?date=" + yesterday + "&job_id=" + config['job_id'] + "&unit_id=" +
        config['unit_id'] + "&field=" + config['field'] +
        "&aggregation_function=" + config['aggregation_function'] + "&aggregation_time=" + config['aggregation_time'])
    res = res.json()
    yesterdayRmse = res["rmse"]

    # 증감률 계산
    if yesterdayRmse == -1 or runningDateRmse == -1:
        increaseRate = -1000
    elif runningDateRmse == 0 and yesterdayRmse != -1:
        increaseRate = -100
    else:
        increaseRate = (runningDateRmse - yesterdayRmse) / runningDateRmse * 100

    # 계산된 증감률 저장
    indexName = "ml_result_" + runningDate
    body = {
        "job_id": config['job_id'],
        "job_type": "forecast_score",
        "unit_id": config['unit_id'],
        "log_time": runningDate + "000000",
        "increase_rate": increaseRate
    }

    res = es.index(index=indexName, doc_type="_doc", body=body)
    print(res)


def anomalyScore(config, runningDate):
    yesterday = dateToString(stringToDate(runningDate) - datetime.timedelta(days=1))
    increaseRate = 0

    # runningDate 날짜의 severity_score 가져오기
    # 반환 형식
    # {"severity_score": {severity_score}}
    res = requests.get(
        "http://192.168.1.60:9999/lcnet/ml/anomaly_score?date=" + runningDate + "&job_id=" + config[
            'job_id'] + "&detection_window=" + config['detection_window'] +
        "&aggregation_time=" + config['aggregation_time'])
    res = res.json()
    runningDateScore = res["severity_score"]

    # runningDate -1일 날짜의 severity_score 가져오기
    res = requests.get(
        "http://192.168.1.60:9999/lcnet/ml/anomaly_score?date=" + runningDate + "&job_id=" + config[
            'job_id'] + "&detection_window=" + config['detection_window'] +
        "&aggregation_time=" + config['aggregation_time'])
    res = res.json()
    yesterdayScore = res["severity_score"]

    # 증감률 계산
    if yesterdayScore == -1 or runningDateScore == -1:
        increaseRate = -1000
    elif runningDateScore == 0 and yesterdayScore != -1:
        increaseRate = -100
    else:
        increaseRate = (runningDateScore - yesterdayScore) / runningDateScore * 100

    # 계산된 증감률 저장
    indexName = "ml_result_" + runningDate
    body = {
        "job_id": config['job_id'],
        "job_type": "anomaly_score",
        "unit_id": config['unit_id'],
        "log_time": runningDate + "000000",
        "increase_rate": increaseRate
    }

    res = es.index(index=indexName, doc_type="_doc", body=body)
    print(res)


def outlierScore(config, runningDate):
    yesterday = dateToString(stringToDate(runningDate) - datetime.timedelta(days=1))
    increaseRate = 0

    # runningDate 날짜의 severity_score 가져오기
    # 반환 형식
    # {"severity_score": {severity_score}}
    res = requests.get(
        "http://192.168.1.60:9999/lcnet/ml/outlier_score?date=" + runningDate + "&job_id=" + config[
            'job_id'] + "&detection_window=" + config['detection_window'] +
        "&aggregation_time=" + config['aggregation_time'])
    res = res.json()
    runningDateScore = res["severity_score"]

    # runningDate -1일 날짜의 severity_score 가져오기
    res = requests.get(
        "http://192.168.1.60:9999/lcnet/ml/outlier_score?date=" + runningDate + "&job_id=" + config[
            'job_id'] + "&detection_window=" + config['detection_window'] +
        "&aggregation_time=" + config['aggregation_time'])
    res = res.json()
    yesterdayScore = res["severity_score"]

    # 증감률 계산
    if yesterdayScore == -1 or runningDateScore == -1:
        increaseRate = -1000
    elif runningDateScore == 0 and yesterdayScore != -1:
        increaseRate = -100
    else:
        increaseRate = (runningDateScore - yesterdayScore) / runningDateScore * 100

    # 계산된 증감률 저장
    indexName = "ml_result_" + runningDate
    body = {
        "job_id": config['job_id'],
        "job_type": "outlier_score",
        "unit_id": config['unit_id'],
        "log_time": runningDate + "000000",
        "increase_rate": increaseRate
    }

    res = es.index(index=indexName, doc_type="_doc", body=body)
    print(res)


@app.route("/joblist")
def jobList():
    jobStr = ""
    jobList = sched.get_jobs()
    for job in jobList:
        print(job)
        jobStr += str(job) + '\n'
    return jobStr


# Start ML Job
@app.route("/ml/config", methods=["post"])
def addJob():
    config = json.loads(request.data.decode('utf8'))
    configObj[config['job_id']] = config
    doML(config)
    return jsonify({"result": True})


# Stop ML Job
@app.route("/ml/config/<job_id>", methods=["delete"])
def removeJob(job_id):
    try:
        # 메모리에서 ML Job 설정 제거
        del (configObj[job_id])

        # 스케줄 제거
        sched.remove_job(job_id=job_id)

        print("Remove Job complete")
        return jsonify({"result": True})
    except Exception as e:
        print(e)
        return jsonify({"result": False})


def doForecast(config, fromTime, toTime):
    returnObj = getTrainingData(config, fromTime, toTime)

    result = forecast.forecast(config, returnObj)

    # 메모리 상의 설정 변경(next_time)
    tempTime = stringToDate(toTime)
    tempTime = tempTime + datetime.timedelta(days=1)
    next_time = dateToString(tempTime)

    config['next_time'] = next_time
    config['job_state'] = "normal"

    configObj[config['job_id']] = config

    # ES 상의 설정 변경 (PUT /lcnet/ml/config/{job_id}?caller=ml)
    body = {
        "next_time": next_time,
        "job_state": "normal"
    }
    headers = {'Content-Type': 'application/json; charset=utf-8'}
    # res = requests.put("http://192.168.1.60:9999/lcnet/config/" + config['job_id'], headers=headers, data=json.dumps(body))
    # print(res.json)

    # ES에 예측 결과 저장
    time = result["returnTimeList"]
    data = result["returnDataList"]
    upper = result["returnDataUpperList"]
    lower = result["returnDataLowerList"]

    bulkList = []
    for i in range(len(time)):
        obj = {
            "_index": "ml_result_" + time[i][0:8],
            "_type": "_doc",
            "_source": {
                "job_id": config["job_id"],
                "job_type": "forecast",
                "job_state": "normal",
                "forecast_value": data[i],
                "confidence_interval_upper": upper[i],
                "confidence_interval_lower": lower[i],
                "unit_id": config['unit_id'],
                "log_time": time[i],
                "failure_message": None
            }
        }
        bulkList.append(obj)
    res = helpers.bulk(es, bulkList)
    print(res)


def getResourceDocumentCount(date, unit_id):
    q = {
        "size": 0,
        "query": {
            "bool": {
                "must": [
                    {
                        "term": {
                            "unit_id": {
                                "value": unit_id
                            }
                        }
                    },
                    {
                        "term": {
                            "sm_type": {
                                "value": "resource"
                            }
                        }
                    }
                ]
            }
        },
        "_source": False,
        "stored_fields": "_none_"
    }

    try:
        res = es.search(index="resource_" + date, body=q)
        return res['hits']['total']
    except elasticsearch.exceptions.NotFoundError as e:
        return 0


def getResource(start, end, function, interval, unit_id, field):
    fullFieldName = ""
    fieldName = ""
    aggFunction = ""
    if field == "cpu":
        fullFieldName = "cpu.cpu_usage"
        fieldName = "cpu"
    elif field == "mem":
        fullFieldName = "mem.mem_usage"
        fieldName = "mem"

    if function == "avg":
        aggFunction = "avg"
    elif function == "sum":
        aggFunction = "sum"
    elif function == "min":
        aggFunction = "min"
    elif function == "max":
        aggFunction = "max"
    elif function == "count":
        aggFunction = "value_count"

    q = {
        "query": {
            "bool": {
                "must": [
                    {
                        "term": {
                            "sm_type": {
                                "value": "resource"
                            }
                        }
                    },
                    {
                        "term": {
                            "unit_id": {
                                "value": unit_id
                            }
                        }
                    },
                    {
                        "range": {
                            "log_time": {
                                "from": start,
                                "to": end
                            }
                        }
                    }
                ],
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    "sm_type": {
                                        "value": "resource",
                                        "boost": 1
                                    }
                                }
                            },
                            {
                                "term": {
                                    "unit_id": {
                                        "value": unit_id,
                                        "boost": 1
                                    }
                                }
                            },
                            {
                                "range": {
                                    "log_time": {
                                        "from": start,
                                        "to": end
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        },
        "aggs": {
            "log_time": {
                "date_histogram": {
                    "field": "log_time",
                    "interval": str(interval) + "m",
                    "offset": 0,
                    "order": {
                        "_key": "asc"
                    },
                    "keyed": False,
                    "min_doc_count": 0
                },
                "aggregations": {
                    fieldName: {
                        "nested": {
                            "path": fieldName
                        },
                        "aggregations": {
                            "usage": {
                                aggFunction: {
                                    "field": fullFieldName
                                }
                            }
                        }
                    }
                }
            },
            fieldName: {
                "nested": {
                    "path": fieldName
                },
                "aggregations": {
                    "usage": {
                        "avg": {
                            "field": fullFieldName
                        }
                    },
                    "usage_min": {
                        "min": {
                            "field": fullFieldName
                        }
                    },
                    "usage_max": {
                        "max": {
                            "field": fullFieldName
                        }
                    }
                }
            }
        }
    }

    res = es.search(index="resource_*", body=q)
    bucketList = res["aggregations"]["log_time"]["buckets"]

    returnObj = {
        "timeList": [data["key_as_string"] for data in bucketList],
        "dataList": [data[fieldName]["usage"]["value"] for data in bucketList]
    }
    return returnObj;


def linearInterpolation(obj, interval, period, startDate):
    timeList = obj['timeList']
    dataList = obj['dataList']

    # 결측 없는 올바른 시간 범위 만들기
    timeRange = pd.date_range(start=startDate, periods=period, freq=str(interval) + "min")
    emptyList = []
    for i in range(int(period)):
        emptyList.append(np.nan)

    df = pd.DataFrame({'correctTime': timeRange, 'data': emptyList})

    for i in range(len(timeList)):
        time = dateToString(stringToDate(timeList[i]))
        index = df['correctTime'][df['correctTime'] == time].index

        df['data'][index] = dataList[i]

    df = df.interpolate(limit_direction='both')

    returnObj = {
        "timeList": [dateToString(date.to_pydatetime()) for date in timeRange],
        "dataList": df['data'].to_list()
    }

    return returnObj


def makeResourceDF(result):
    timeList = result['timeList']
    dataList = result['dataList']
    timeList = [datetime.datetime.strptime(time, "%Y%m%d%H%M%S") for time in timeList]

    df = pd.DataFrame(data={"ds": timeList, "y": dataList})
    df["ds"] = df["ds"].astype("datetime64[ns]")

    df['cap'] = 100
    df['floor'] = 0

    return df


def analyzer(df):
    std = df.describe()['y']['std']

    if std > 5:
        return "active"
    else:
        return "idle"


# 최적화모드 추가 개발 필요
def getTrainingData(config, trainFrom, trainTo):
    notEnough = False
    last7DaysObjList = []  # 현재시간 기준으로 7일 전까지의 리소스 데이터를 검색해 저장하는 리스트

    workingDayList = []
    idleDayList = []

    # 만약 과거 7일치 데이터가 온전히 없다면 최적화 모드 없음
    for i in range(1, 8):
        docCount = getResourceDocumentCount((stringToDate(trainTo) - datetime.timedelta(days=i)).strftime("%Y%m%d"),
                                            config['unit_id'][0])
        returnObj = getResource((stringToDate(trainTo) - datetime.timedelta(days=i)).strftime("%Y%m%d000000"),
                                (stringToDate(trainTo) - datetime.timedelta(days=i)).strftime("%Y%m%d235959"),
                                config['aggregation_function'], config['aggregation_time'], config['unit_id'][0],
                                config['field'])

        last7DaysObjList.insert(0, returnObj)

        if docCount < 720:
            notEnough = True
            break

    # 과거 7일치 데이터가 없을 경우 인접한 날짜의 데이터만 이용
    if notEnough == True:
        returnObj = getResource(trainFrom, trainTo, config['aggregation_function'], config['aggregation_time'],
                                config['unit_id'][0], config['field'])

        #####################[검증필요]#########################
        # 이쯤에서 6시간보다 데이터 적은지 판단 필요
        returnObj = linearInterpolation(returnObj, config['aggregation_time'], int(
            1440 / config['aggregation_time'] * (stringToDate(trainTo) - stringToDate(trainFrom)).days), trainFrom)

        return returnObj
    #####################[검증필요]#########################
    elif notEnough == False:
        # 직접 과거 7일치 데이터를 분석해서 working day를 구분해야만 함
        if len(config['working_day']) == 0:
            for obj in last7DaysObjList:
                df = makeResourceDF(obj)
                weekday = stringToDate(obj['timeList'][0]).weekday()
                state = analyzer(df)
                print("weekday: " + str(weekday) + ", state: " + state)
                if state == "active":
                    workingDayList.append(weekday)
                elif state == "idle":
                    idleDayList.append(weekday)
        # 이미 config['working_day']에 working_day가 월요일부터 순서대로 true, false로 담겨져있음
        else:
            working_day = config['working_day']
            for i in range(7):
                if working_day[i] == True:
                    workingDayList.append(i)
                else:
                    idleDayList.append(i)

        # 일단 설정대로 학습 데이터 전체를 먼저 가져온다. 그 다음에 요일에 따라 필요한 날의 데이터만 이용한다.
        returnObj = getResource(trainFrom, trainTo, config['aggregation_function'], config['aggregation_time'],
                                config['unit_id'][0], config['field'])

        todayWeekday = datetime.datetime.now(timezone("UTC")).weekday()
        # 여기부턴 이미 workingDayList와 idleDayList에 분석 결과가 존재함
        # 추후 개발


def forecastJob(job_id):
    config = configObj[job_id]
    toTime = stringToDate(config['next_time'])
    fromTime = toTime - datetime.timedelta(days=config['training_period'])

    doForecast(config, dateToString(fromTime), dateToString(toTime))


def outlierJob(job_id):
    print("outlierJob called")
    config = configObj[job_id]
    toTime = stringToDate(config['next_time'])
    fromTime = toTime - datetime.timedelta(minutes=config['aggregation_time'])

    doOutlier(config, dateToString(fromTime), dateToString(toTime))


def doOutlier(config, fromTime, toTime):
    print("fromTime: " + fromTime + ", toTime: " + toTime)
    insufficientHostNameList = []

    dataObj = {}
    # UTC 적용하기 전 테스트용 임시 절차
    # fromTime = dateToString(stringToDate(fromTime) - datetime.timedelta(hours=9))
    # toTime = dateToString(stringToDate(toTime) - datetime.timedelta(hours=9))
    next_time = dateToString(stringToDate(toTime) + datetime.timedelta(minutes=config['aggregation_time']))

    # 타겟 호스트들의 현재 데이터를 가져오기
    for unit_id in config['unit_id']:
        try:
            # 만약 아무 호스트중 하나라도 데이터가 없다면 실패처리
            result = getResource(fromTime, toTime, config["aggregation_function"], config['aggregation_time'], unit_id,
                                 config["field"])
            dataObj[str(unit_id)] = result["dataList"][-1]
            time = result["timeList"][-1]
            print("unit_id: " + str(unit_id) + ",data: " + str(result["dataList"][-1]))
        except IndexError:
            print("unit_id: " + str(unit_id) + ", data insufficient")
            insufficientHostNameList.append(config['hostnm'][config['unit_id'].index(unit_id)])

    # 만약 아무 호스트중 하나라도 데이터가 없다면 실패처리
    if len(insufficientHostNameList) != 0:
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        body = {
            "next_time": next_time,
            "job_state": "fail"
        }
        # res = requests.put("http://192.168.1.60:9999/lcnet/ml/config/" + config['job_id'] +"?caller=ml", headers=headers, data=json.dumps(body))
        # print(res.json)

        body = {
            "job_id": config['job_id'],
            "job_type": "outlier",
            "job_state": "fail",
            "log_time": toTime,
            "isOutlier": None,
            "result": None,
            "failure_message": "Insufficient data(host name: " + str(insufficientHostNameList) + ")"
        }

        res = es.index("ml_result_" + toTime[0:8], doc_type="_doc", body=body)
        print(res)

        # 작업 비정상 종료
        config['job_state'] = "fail"

    else:
        parameterObj = {
            "m": config["m"],
            "e": config["e"],
            "data": dataObj
        }

        print("parameterObj")
        print(parameterObj)

        # 아웃라이어 탐지 시작
        returnObj = outlier.outlier(parameterObj)
        print(returnObj)

        isOutlier = returnObj['isOutlier']

        del (returnObj['isOutlier'])

        # ES에 저장
        body = {
            "job_id": config['job_id'],
            "job_type": "outlier",
            "log_time": toTime,
            "isOutlier": isOutlier,
            "failure_message": None,
            "result": returnObj
        }

        if isOutlier:
            body['job_state'] = "warning"
        else:
            body['job_state'] = "normal"

        print(body)
        res = es.index("ml_result_" + toTime[0:8], doc_type="_doc", body=body)
        print(res)

        # 작업 정상 종료
        config['job_state'] = body['job_state']

    # 메모리에 설정 변경
    config['next_time'] = next_time
    configObj[config['job_id']] = config

    print(config)
    # ES 상의 설정 변경 (PUT /lcnet/ml/config/{job_id}?caller=ml)
    body = {
        "next_time": next_time,
        "job_state": body['job_state']
    }
    headers = {'Content-Type': 'application/json; charset=utf-8'}
    # res = requests.put("http://192.168.1.60:9999/lcnet/config/" + config['job_id'], headers=headers, data=json.dumps(body))
    # print(res.json)


def doAnomaly(config, fromTime, toTime):
    pastObjList = []

    # ES에서 오늘 날짜 기준 실제 값 가져오기
    # UTC 적용하기 전 테스트용 임시 절차
    # fromTime = dateToString(stringToDate(fromTime) - datetime.timedelta(hours=9))
    # toTime = dateToString(stringToDate(toTime)-datetime.timedelta(hours=9))
    next_time = dateToString(stringToDate(toTime) + datetime.timedelta(minutes=config['aggregation_time']))

    todayObj = getResource(fromTime, toTime, config['aggregation_function'], config['aggregation_time'],
                           config['unit_id'][0], config['field'])
    try:
        todayObj = {
            'timeList': todayObj['timeList'][-1],
            'dataList': todayObj['dataList'][-1]
        }
        print(todayObj)
    except:
        pass

    # 과거 데이터 최대 3일치 가져오기
    for i in range(1, 4):
        pastObj = getResource(dateToString(stringToDate(fromTime) - datetime.timedelta(days=i)),
                              dateToString(stringToDate(toTime) - datetime.timedelta(days=i)),
                              config['aggregation_function'], config['aggregation_time'], config['unit_id'][0],
                              config['field'])
        try:
            pastObj = {
                'timeList': pastObj['timeList'][-1],
                'dataList': pastObj['dataList'][-1]
            }
        except:
            pass

        if len(pastObj['timeList']) == 0:
            continue
        else:
            pastObjList.append(pastObj)

    # 만약 오늘 날짜 기준 실제값이 없거나 과거 데이터의 개수가 2개보다 작다면 작업 실패 처리
    if len(pastObjList) < 2 or len(todayObj['timeList']) == 0:
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        body = {
            "next_time": next_time,
            "job_state": "fail"
        }
        # res = requests.put("http://192.168.1.60:9999/lcnet/ml/config/" + config['job_id'] +"?caller=ml", headers=headers, data=json.dumps(body))
        # print(res.json)

        body = {
            "job_id": config['job_id'],
            "job_type": "anomaly",
            "job_state": "fail",
            "real_value": None,
            "confidence_interval_lower": None,
            "confidence_interval_upper": None,
            "unit_id": config['unit_id'],
            "isAnomaly": None,
            "log_time": toTime,
            "failure_message": "Insufficient data"
        }

        res = es.index("ml_result_" + toTime[0:8], doc_type="_doc", body=body)
        print(res)

        # 작업 비정상 종료
        config['job_state'] = "fail"
    else:
        # DBSCAN 적용
        resultObj = anomaly.anomaly(config, todayObj, pastObjList)
        print(resultObj)

        # ES에 저장
        body = {
            "job_id": config['job_id'],
            "job_type": "anomaly",
            "real_value": resultObj['real_value'],
            "confidence_interval_upper": resultObj['upper'],
            "confidence_interval_lower": resultObj['lower'],
            "unit_id": config['unit_id'],
            "isAnomaly": resultObj['isAnomaly'],
            "log_time": toTime,
            "failure_message": None
        }

        if resultObj['isAnomaly']:
            body['job_state'] = "warning"
        else:
            body['job_state'] = "normal"
        print(body)
        res = es.index("ml_result_" + toTime[0:8], doc_type="_doc", body=body)
        print(res)

        # 작업 정상 종료
        config['job_state'] = body['job_state']

    # 메모리에 설정 변경
    config['next_time'] = next_time
    configObj[config['job_id']] = config

    # ES 상의 설정 변경 (PUT /lcnet/ml/config/{job_id}?caller=ml)
    body = {
        "next_time": next_time,
        "job_state": body['job_state']
    }
    headers = {'Content-Type': 'application/json; charset=utf-8'}
    # res = requests.put("http://192.168.1.60:9999/lcnet/config/" + config['job_id'], headers=headers, data=json.dumps(body))
    # print(res.json)


def anomalyJob(job_id):
    print("anomalyJob called")
    config = configObj[job_id]
    toTime = stringToDate(config['next_time'])
    fromTime = toTime - datetime.timedelta(minutes=config['aggregation_time'])

    doAnomaly(config, dateToString(fromTime), dateToString(toTime))


def doML(config):
    job_type = config['job_type']
    if job_type == "forecast":
        if (config['next_time'] == None) or (
                stringToDate(config['next_time']) < datetime.datetime.now(timezone("UTC"))):
            print("Forecast Job Starting...")
            fromTime = (datetime.datetime.now(timezone("UTC")) - datetime.timedelta(
                days=config['training_period'])).strftime("%Y%m%d000000")
            toTime = datetime.datetime.now(timezone("UTC")).strftime("%Y%m%d000000")

            doForecast(config, fromTime, toTime)

            print("add to scheduler...")
            sched.add_job(forecastJob, "interval", days=1, id=config['job_id'], args=[config['job_id']],
                          start_date=(stringToDate(toTime) - datetime.timedelta(days=1)), max_instances=1000,
                          misfire_grace_time=300)
        elif stringToDate(config['next_time']) > datetime.datetime.now(timezone("UTC")):
            print("add to scheduler...")
            sched.add_job(forecastJob, "interval", days=1, id=config['job_id'], args=[config['job_id']],
                          start_date=(stringToDate(config['next_time']) - datetime.timedelta(days=1)),
                          max_instances=1000, misfire_grace_time=300)
    elif job_type == "anomaly":
        if (config['next_time'] == None) or (
                stringToDate(config['next_time']) < datetime.datetime.now(timezone("UTC"))):
            print("anomaly detection Job Starting...")
            toTime = datetime.datetime.now(timezone("UTC")).strftime("%Y%m%d%H%M00")  # 현재 시간 (분까지만)
            fromTime = (datetime.datetime.now(timezone("UTC")) - datetime.timedelta(
                minutes=config['aggregation_time'])).strftime(
                "%Y%m%d%H%M00")

            doAnomaly(config, fromTime, toTime)

            print("add to scheduler...")
            sched.add_job(anomalyJob, "interval", minutes=config['aggregation_time'], id=config['job_id'],
                          args=[config['job_id']], max_instances=1000, misfire_grace_time=300)
        elif stringToDate(config['next_time']) > datetime.datetime.now(timezone("UTC")):
            print("add to scheduler...")
            sched.add_job(anomalyJob, "interval", minutes=config['aggregation_time'], id=config['job_id'],
                          args=[config['job_id']],
                          start_date=config['next_time'] - datetime.timedelta(minutes=config["aggregation_time"]),
                          max_instances=1000, misfire_grace_time=300)

    elif job_type == "outlier":
        if (config['next_time'] == None) or (
                stringToDate(config['next_time']) < datetime.datetime.now(timezone("UTC"))):
            print("outlier detection Job Starting...")
            toTime = datetime.datetime.now(timezone("UTC")).strftime("%Y%m%d%H%M00")  # 현재 시간 (분까지만)
            fromTime = (datetime.datetime.now(timezone("UTC")) - datetime.timedelta(
                minutes=config['aggregation_time'])).strftime(
                "%Y%m%d%H%M00")

            doOutlier(config, fromTime, toTime)

            print("add to scheduler...")
            sched.add_job(outlierJob, "interval", minutes=config['aggregation_time'], id=config['job_id'],
                          args=[config['job_id']], max_instances=1000, misfire_grace_time=300)
        elif stringToDate(config['next_time']) > datetime.datetime.now(timezone("UTC")):
            print("add to scheduler...")
            sched.add_job(outlierJob, "interval", minutes=config['aggregation_time'], id=config['job_id'],
                          args=[config['job_id']],
                          start_date=config['next_time'] - datetime.timedelta(minutes=config["aggregation_time"]),
                          max_instances=1000, misfire_grace_time=300)


def init():
    # API로부터 모든 설정을 받아오기
    # GET /lcnet/ml/all_config
    res = requests.get("http://192.168.1.60:9999/lcnet/ml/all_config")
    res = res.json()

    allConfig = res

    # 모든 ML Job 설정에 대해 작업 시도
    for config in allConfig:
        # 만약 job_state가 OFF인 설정이 있다면 스케줄러는 만들지 않음
        if config['job_state'] == "off":
            continue
        else:
            # 메모리에 설정 저장
            configObj[config['job_id']] = config
            doML(config)


if __name__ == "__main__":
    executors = {
        'default': {
            'type': 'threadpool',
            'max_workers': 1
        }
    }

    sched = BackgroundScheduler()
    sched.configure(executors=executors, timezone=utc)

    sched.start()
    # init()

    app.run(host="0.0.0.0", port="4000")